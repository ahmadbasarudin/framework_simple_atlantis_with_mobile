<?php
/*
|--------------------------------------------------------------------------
| Config
|--------------------------------------------------------------------------
|
|Config system
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    DEFINE("DB_USER" , "root");
    DEFINE("DB_PASSWORD" ,"");
    DEFINE("DB_NAME" ,"simple_mobile_db");
    DEFINE("DB_HOST" , "localhost");

    DEFINE("SUPER_ADMIN_GROUP","SUPER_ADMINISTRATOR");
    DEFINE("LOGIN_REFERENCE","PASSWORD");


    $SYSTEM['DIR_PATH'] = "/Applications/XAMPP/htdocs/framework_simple_atlantis_with_mobile/SERVER/API";

    $SYSTEM['DIR_MODUL_CLASS'] = "/Applications/XAMPP/htdocs/framework_simple_atlantis_with_mobile/SERVER/APP" ."/class";
    //$SYSTEM['DIR_PATH'] = "/opt/lampp/htdocs/simontok-api";
    //$SYSTEM['DIR_PATH'] = "D:/xampp/htdocs/simontok-api";
    $SYSTEM['DIR_MODUL'] = $SYSTEM['DIR_PATH'] ."/modul";
    $SYSTEM['DIR_MODUL_CORE'] = $SYSTEM['DIR_MODUL'] ."/core";


    $SYSTEM['FIREWALL'] ='OFF'; //OFF, ON


?>