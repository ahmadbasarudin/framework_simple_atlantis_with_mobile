<?php


    /*
    |--------------------------------------------------------------------------
    | Secure Enhanced
    |--------------------------------------------------------------------------
    |Digunakan setiap kali transaksi sesudah login
    |Hanya berkaitan dengan keamanan akses file diluar alamat karena keamanan alamat sudah ditangani firewall
    |
    |
    |diperbolehkan siapa saja yang mengakses dengan membandingkan page_id dicocokan dengan database
    */

    include_once("config.php");
    require_once($SYSTEM['DIR_MODUL_CLASS']."/function.array.php");
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.user.php");

    //mencari userID
    $USER = $oUserInfo->getUserList(" WHERE firebaseToken = '{$_POST['TOKEN_KEY']}' ","","");
    if(count($USER) > 0 )
    {
        if($USER[0]['active'] == 0)
        {
            $_REQUEST['error_code'] = "pengguna belum aktif";
        }
        elseif($USER[0]['active'] == 2)
        {

            $_REQUEST['error_code'] = "pengguna diblokir";
        }
        else
        {
            $PRIVILEGES=  $oUserInfo->getUserPrivileges($USER[0]['userID']);
            if(deepInArray($PAGE_ID,$PRIVILEGES))
            {
                // halaman telah lolos validasi
            }
            else
            {
                //tolak dan dibawa ke index.php karena tidak mempunyai hak akses untuk halaman ini
                $_REQUEST['error_code'] = $PAGE_ID.".Anda tidak mempunyai hak untuk mengakses halaman ini.";
            }        

        }
    }
    else
    {
        $_REQUEST['error_code'] = "data pengguna tidak ditemukan";
    }
    if(isset($_REQUEST['error_code']))
    {
        require($SYSTEM['DIR_MODUL_CORE']."/denied.php");
    }

    $oUserInfo->closeDB();
?>