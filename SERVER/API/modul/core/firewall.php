<?php
    /*
    |--------------------------------------------------------------------------
    | Firewall
    |--------------------------------------------------------------------------
    |Hanya digunakan untuk sekali login
    |Hanya berkaitan dengan keamanan alamat
    |
    |
    |diperbolehkan siapa saja yang mengakses dengan membandingkan ip address / imei sewaktu login
	|Digunakan untuk membuat log
	|prefix parameter pada class:
	|     _ :  parameter 
	|     i :  integer 
	|     b :  boolean 
	|     a :  array 
	|     s :  string
    */
	

	$PAGE_ID="LOGIN";
	include_once("config.php");
	require_once($SYSTEM['DIR_MODUL_CLASS']."/class.client.php");
	require_once($SYSTEM['DIR_MODUL_CLASS']."/class.firewall.php");
	require_once($SYSTEM['DIR_MODUL_CLASS']."/class.notification.php");
	require_once($SYSTEM['DIR_MODUL_CLASS']."/class.user.php");
	$oClient = new Client();
	$oFirewall = new Firewall();
	$oNotification = new Notification();
	$oUser = new UserInfo();

	//digunakan untuk menotifikasi semua admin  jika ada pengguna yang kena blok firewall
	$group_admin = 'SUPER_ADMINISTRATOR';
	if(isset($SYSTEM['FIREWALL']))
	{
		//jika on
		if(strtoupper($SYSTEM['FIREWALL']) ==  "ON")
		{
			//dapatkan jenis client. jika menggunakan pc maka firewall menggunakan IP. tetapi jika android maka menggunakan imei . imei didapat dari post client android
			//jika dari android
			if(isset($_REQUEST['IMEI']))
			{
				$CLIENT_ADDRESS  = $_REQUEST['IMEI'];
			}
			else
			{
				$CLIENT_ADDRESS =$oClient->getClientIP();
			}

			if(isset($CLIENT_ADDRESS))
			{
				$condition = " WHERE clientAddress = '{$CLIENT_ADDRESS}' ";
				if($oFirewall->getCount($condition) > 0)
				{
				    $result['result'] = 'success';
				    $result['desc'] = "akses firewall diterima";
				}
				else
				{
				    $result['result'] = 'error';
				    $result['desc'] = "anda tidak berhak mejalankan sistem dari tempat / alamat ini. <br /> hubungi administrator untuk mengupdate firewall akses  anda.";

				    /*
				    |notifikasi ke semua user admin
				    */
					$a_user = $oUser->getUserByGroup(SUPER_ADMIN_GROUP);
					if(isset($a_user))
					{
						if(count($a_user) > 0)
						{

		                    foreach ($a_user as $value) 
		                    {
								$notif_user_to =  $value['userID']; 
								$notif_title  = "akses sistem ditolak";
								$notif_message = "akses pengguna ditolak dengan alamat {$CLIENT_ADDRESS} karena tidak  ditemukan di akses firewall.";
								$notif_link  = "index.php/firewall/list";
								
								$oNotification->create(
				                    $notif_title ,
				                    $notif_message ,
				                    $notif_link ,
				                    $notif_user_to ,
				                    "" ,
				                    "" ,
				                    "" 
								);
		                    }
						}
					}


				}
			}
			else
			{
			    $result['result'] = 'error';
			    $result['desc'] = "alamat client tidak dapat diterjemahkan";
			}


		}
		//jika off maka tidak dieksekusi
	}
	else
	{
	    $result['result'] = 'error';
	    $result['desc'] = "konfigurasi firewall belum disetting di config";
	}

	$oFirewall->closeDB();
	$oNotification->closeDB();
	$oUser->closeDB();
?>