<?php
/**
 *
 * @version   1.0
 * @author    basarudin
 * @created     Juli 05 ,2015
 * @log
 *
 * prefix parameter:
 *    n  - node
 *    o  - object
 *    a  - array
 *    s  - string
 *    b  - boolean
 *    f  - float
 *    i  - integer
 *    fn - function
 *    _  - parameter
 *   penulisan variabel pemisah = _
 *   penulisan variabel untuk dipakai disemua halaman menggunakan huruf besar semua contoh $USER;
 *   penulisan method huruf pertama kecil selanjutnya besar
 **/

    
    $respone['status'] = "error";
    $respone['desc'] = "anda tidak dapat mengakses halaman ini.";

    if(isset($_REQUEST['error_code']))
    {
        $respone['desc'] .= "kode error : ".$_REQUEST['error_code'];
    }

    echo json_encode($respone);

?>