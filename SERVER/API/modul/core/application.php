<?php
/*
|--------------------------------------------------------------------------
| Application Model
|--------------------------------------------------------------------------
|
|Model application
|    
|Digunakan untuk membuat model data login
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

	$PAGE_ID="APPLICATION";
    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.app.php");
	//require_once($SYSTEM['DIR_PATH']."/class/class.app.php");
	$oApp = new Application();
	//jika variable error sudah terisi maka kosongkanlah
	$respone['status'] = "error";
	$respone['desc'] = "tidak tereksekusi";

	$sError  = "";
    $a_errors = array();

	if(isset($_REQUEST['action']))
	{
		if($_REQUEST['action'] == "version")
		{
			if(!isset($_REQUEST['appName']))
            {
                $a_errors[] = "ID applikasi tidak ada";
            }

            if (!$a_errors) 
            {
            	//cek apakah ada di pengguna
                $s_where = " WHERE appName = '{$_REQUEST['appName']}' ";
                if($oApp->getCount($s_where)> 0)
                {                    $a_data = $oApp->getList($s_where, "", "");
                    $respone['status'] = 'success';
                    $respone['desc'] = "";
                    $respone['version'] = $oApp->getVersion($s_where);	                	
                }
                else
                {
                    $respone['status'] = 'error';
                    $respone['desc'] = "app id tidak ditemukan";
                }
            }
		}
	}

    if ($a_errors) 
    {
    	$sError =  '';
        foreach ($a_errors as $error) 
        {
            $sError .= "$error<br />";
        }
        $respone['status'] = 'error';
        $respone['desc'] = $sError;
    }
	$oApp->closeDB();

?>