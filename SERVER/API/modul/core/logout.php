<?php
/**
 * logout.php
 *
 * @version         1.0
 * @author          basarudin
 * @created         21 april 2010
 *
 * digunakan untuk melogout session login
 **/
	require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
	$oUser = new UserInfo();
    $oUserInfo->logout();
    header("Location:index.php");
?>