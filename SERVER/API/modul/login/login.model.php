<?php
/*
|--------------------------------------------------------------------------
| Login Model
|--------------------------------------------------------------------------
|
|Model login
|    
|Digunakan untuk membuat model data login
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

	$PAGE_ID="LOGIN";
	require_once($SYSTEM['DIR_MODUL_CLASS']."/class.user.php");
	require_once($SYSTEM['DIR_MODUL_CLASS']."/class.client.php");
	require_once($SYSTEM['DIR_MODUL_CLASS']."/class.log.php");
	require_once($SYSTEM['DIR_MODUL_CLASS']."/class.firewall.php");
	require_once($SYSTEM['DIR_MODUL_CLASS']."/class.device.php");
	require_once($SYSTEM['DIR_MODUL_CLASS']."/class.app.php");
	$oApp = new Application();
	$oUser = new UserInfo();
	$oLog = new Log();
	$oClient = new Client();
	$oDevice = new Device();
	//jika variable error sudah terisi maka kosongkanlah
	$respone['status'] = "error";
	$respone['desc'] = "";
	$s_error  = "";
    $a_errors = array();
 	if(isset($_REQUEST['action']))
	{
		if($_REQUEST['action'] == "register-device"){


			if($_REQUEST['app-name'] == "")
            {
                $a_errors[] = "nama aplikasi tidak ada";
            }
			if($_REQUEST['app-version'] == "")
            {
                $a_errors[] = "versi aplikasi tidak ada";
            }


			if($_REQUEST['user-id'] == "")
            {
                $a_errors[] = "nip tidak ada";
            }
            if($_REQUEST['email'] == "")
            {
                $a_errors[] = "email tidak boleh kosong";
            }
            if($_REQUEST['firebase-token'] == "")
            {
                $a_errors[] = "firebase tidak boleh kosong";
            }
            if($_REQUEST['device-model'] == "")
            {
                $a_errors[] = "device model tidak boleh kosong";
            }
            if($_REQUEST['device-sdk'] == "")
            {
                $a_errors[] = "device sdk tidak boleh kosong";
            }
            if($_REQUEST['device-merk'] == "")
            {
                $a_errors[] = "merk hp tidak boleh kosong";
            }





            if (!$a_errors) 
            {

            	//periksa nama dan versi aplikasi apakah sudah ada disistem
                $s_where_app = " WHERE appName = '{$_REQUEST['app-name']}' ";
                $a_data_app = $oApp->getList($s_where_app,"","");
                if(isset($a_data_app))
                {
                	if(count($a_data_app)>0)
                	{
                		if($_REQUEST['app-version'] >= $a_data_app[0]['lastVersion'])
                		{

							//cek apakah ada di pengguna
			                $s_where_user = " WHERE `userID` = '{$_REQUEST['user-id']}' AND mail = '{$_REQUEST['email']}' ";
			                if($oUser->getUserCount($s_where_user)> 0)
			                {

				            	$s_where_device = " WHERE `userID` = '{$_REQUEST['user-id']}' AND deviceModel =   '{$_REQUEST['device-model']}' AND registerStatus = 0";
				            	if($oDevice->getCount($s_where_device) > 0)
				            	{

				                    $respone['status'] = 'error';
				                    $respone['desc'] = "device ini sudah pernah didaftarkan dan sedang menunggu aktifasi dari helpdesk";
				            	}
				            	else
				            	{
									//membuat register untuk diantrikn
				                	if($oDevice->create(
				                		$_REQUEST['app-name'],
				                		$_REQUEST['app-version'],
				                		$_REQUEST['user-id'],
				                		$_REQUEST['firebase-token'],
				                		$_REQUEST['device-model'],
				                		$_REQUEST['device-sdk'],
				                		$_REQUEST['device-merk'],
				                		$_REQUEST['device-version'],
				                		$_REQUEST['device-pixel'],
				                		$_REQUEST['device-dpi'],
				                		$_REQUEST['device-inch'],
				                		$_REQUEST['device-country'],
				                		$_REQUEST['device-language'],
				                		$_REQUEST['device-uuid'],
				                		$_REQUEST['device-android-id'],
				                		$_REQUEST['device-network-carrier'],
				                		$_REQUEST['device-network-id']


				                		 ))
									{
					                    $respone['status'] = 'success';
					                    $respone['desc'] = "pengguna sudah dimasukan dalam antrian akun";
				                		$respone['token_login'] = "";
				                		$respone['register_status'] = "0";
									}
									else
									{

					                    $respone['status'] = 'error';
					                    $respone['desc'] = "gagal dalam memasukan antrian akun";
									}
				            	}
				            	
				                	
			                }
			                else
			                {
			                    $respone['status'] = 'error';
			                    $respone['desc'] = "kombinasi nik dan email salah";

			                } 

                		}
                		else
                		{
		                    $respone['status'] = 'error';
		                    $respone['desc'] = "versi aplikasi sudah kadaluarsa. harap download apk yang terbaru.";

                		}
                	}
                	else
                	{

	                    $respone['status'] = 'error';
	                    $respone['desc'] = "aplikasi ini tidak terdaftar didalam sistem. harap mengunduh apk resmi dari helpdesk.";
                	}
                }
                else
                {
                    $respone['status'] = 'error';
                    $respone['desc'] = "aplikasi yang anda install bukan resmi dari helpdesk. harap download dan install ulang aplikasi resminya";

                }

			            	
            }

		}

        elseif ($_REQUEST['action'] == 'imei-check') {
            # code...

            $s_where_device = " WHERE DUR.`carrierID` = '{$_REQUEST['device-network-carrier']}'  ";
            $a_data  = $oDevice->getList($s_where_device,"","");
            if(count($a_data ) > 0)
            {
                if($a_data[0]['userID'] == $_REQUEST['user-id']  && $a_data[0]['mail'] == $_REQUEST['email'] )
                {
                    //sudah aktifasi
                    if($a_data[0]['registerStatus']  == "1")
                    {
                        $respone['token-login'] = $a_data[0]['loginToken'];
                        $respone['desc'] = "akun sudah aktif";

                    }
                    else
                    {
                        $respone['desc'] = "menunggu aktifasi admin";

                    }

                    if
                    (
                        //update firebase token
                        $oDevice->updateFirebaseToken($a_data[0]['durID'],$_REQUEST['firebase-token'])
                        &&
                        //ganti status device register
                        $oDevice->activationDeviceRegister($a_data[0]['durID'],"2")
                    )
                    {
                        $respone['status'] = 'success';
                        $respone['real-name'] = $a_data[0]['realName'];

                    }
                    else
                    {
                        $respone['status'] = 'error';
                        $respone['desc'] = "perangkat ini sudah pernah dikaitkan dengan akun lain. system tidak bisa mendeteksi perangkat baru ";

                    }

                }
                else
                {
                    $respone['status'] = 'error';
                    $respone['desc'] = "perangkat ini sudah pernah dikaitkan dengan akun lain.harap periksa ulang pengguna dan email jika memang perangkat ini milik anda. ";
                }
            }
            else
            {
                $respone['status'] = 'error';
                $respone['desc'] = "imei tidak terdaftar didatabase";

            }

        }

		elseif($_REQUEST['action'] == 'check-waiting')
		{
			if($_REQUEST['firebase-token'] == "")
            {
                $a_errors[] = "firebase token masih kosong";
            }
            if (!$a_errors) 
            {
            	$s_where_device = " WHERE DUR.`firebaseToken` = '{$_REQUEST['firebase-token']}' ";
                //echo $s_where_device ;
            	$a_data  = $oDevice->getList($s_where_device,"","");
				if(count($a_data ) > 0)
            	{
            		if($a_data[0]['registerStatus'] == 1)
            		{
            			$respone['status'] = 'success';
	                    $respone['token-login'] = $a_data[0]['loginToken'];
	                    $respone['desc'] = "akun sudah aktif";
                		$respone['register-status'] = "1";
                		$respone['realname'] = $a_data[0]['realName'];
            		}
            		elseif($a_data[0]['registerStatus'] == 0)
            		{

	                    $respone['status'] = 'error';
	                    $respone['desc'] = "register anda sudah masuk dalam daftar tunggu. harap menghubungi helpdesk";
            		}
                    elseif($a_data[0]['registerStatus'] == 2)
                    {

                        $respone['status'] = 'error';
                        $respone['desc'] = "dikarenakan anda menginstal ulang / akun dinonaktifkan, anda harus mengunggu admin untuk mengaktifkan ulang";
                    }
            		else
            		{
    					$respone['status'] = 'error';
	                    $respone['desc'] = "akun anda sudah expired. harap install ulang aplikasi anda";
            		
            		}
            	}
            	else
            	{
                    $respone['status'] = 'error';
                    $respone['desc'] = "firebase tidak terdaftar didatabase";

            	}
            }
		}

		

	}

    if ($a_errors) 
    {
    	$s_error =  '';
        foreach ($a_errors as $error) 
        {
            $s_error .= "$error<br />";
        }
        $respone['status'] = 'error';
        $respone['desc'] = $s_error;
    }
	$oUser->closeDB();
	$oLog->closeDB();
	$oDevice->closeDB();
	$oApp->closeDB();

?>