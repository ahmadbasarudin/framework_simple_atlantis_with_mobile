<?php
    /*
    |--------------------------------------------------------------------------
    | Index
    |--------------------------------------------------------------------------
    |Halaman utama 
    |Berfungsi sebagai pemaggil init, route sekaligus autoload dari fungsi fungsi
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    |
    */
    session_start();
	include_once("config.php");
    include_once($SYSTEM['DIR_MODUL_CORE']."/init.php");
    include_once($SYSTEM['DIR_MODUL_CORE']."/route.php");
    //masukan modul yang dibutuhkan untuk pre modul eksekusi	
    if($CONTENTS != "")
    {
        echo $CONTENTS;   
    }

?>