<?php
/*
|--------------------------------------------------------------------------
| Config
|--------------------------------------------------------------------------
|
|Config system
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    DEFINE("DB_USER" , "root");
    DEFINE("DB_PASSWORD" ,"");
    DEFINE("DB_NAME" ,"simple_mobile_db");
    DEFINE("DB_HOST" , "localhost");

    DEFINE("SUPER_ADMIN_GROUP","SUPER_ADMINISTRATOR");
    DEFINE("LOGIN_REFERENCE","PASSWORD");


    $SYSTEM['APP_NAME'] = "MOBILE"; 
    $SYSTEM['DIR_PATH'] = "/Applications/XAMPP/htdocs/framework_simple_atlantis_with_mobile/SERVER/APP";
    //$SYSTEM['DIR_PATH'] = "D:/xampp/htdocs/simontok/APLIKASI/APP";
    $SYSTEM['DIR_MODUL'] = $SYSTEM['DIR_PATH'] ."/modul";
    $SYSTEM['DIR_MODUL_CLASS'] = $SYSTEM['DIR_PATH'] ."/class";
    $SYSTEM['DIR_MODUL_CORE'] = $SYSTEM['DIR_MODUL'] ."/core";
    $SYSTEM['DIR_MODUL_LAYOUT'] = $SYSTEM['DIR_MODUL'] ."/layout";
    $SYSTEM['DIR_MODUL_UPLOAD'] = $SYSTEM['DIR_PATH'] ."/uploaded";


    //skin-dark 
    //modul skin terletak di css/skins
    $SYSTEM['SKIN'] ='  skin-dark '; //OFF, ON
    $SYSTEM['LOGO'] = "";
    $SYSTEM['FIREWALL'] ='OFF'; //OFF, ON

    //AUDIT KOMPUTER
    //untuk config detail silahkan edit file di modul audit audit.config.php
    $AUDIT['KEEP_CONFIG'] = 'y'; //y or n. file config tidak dihapus setelah eksekusi



    //FIREBASE
    $SYSTEM['FIREBASE_DELAY'] =40; //40detik
    $SYSTEM['FIREBASE_FLAG_SEND'] = 1;
    $SYSTEM['FIREBASE_FLAG_RECEIVED'] = 2;
    $SYSTEM['FIREBASE_FLAG_READ'] = 3;
    $SYSTEM['FIREBASE_FLAG_ERROR'] = 8;
    $SYSTEM['FIREBASE_FLAG_DELETE'] = 9;
    $SYSTEM['FIREBASE_URL'] = 'https://fcm.googleapis.com/fcm/send';
    $SYSTEM['FIREBASE_HEADER'] = array(
            'Authorization:key = AIzaSyANaMm08VOerp97jR6Kxaru73EyAAj_HdE ',
            'Content-Type: application/json'
            );



?>