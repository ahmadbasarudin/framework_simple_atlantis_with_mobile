<?PHP
/**
 *
 * @version		1.0
 * @author 		basarudin
 * @created     Juli 05 ,2015
 * @log
 *
 * prefix parameter:
 *    n  - node
 *    o  - object
 *    a  - array
 *    s  - string
 *    b  - boolean
 *    f  - float
 *    i  - integer
 *    fn - function
 *    _  - parameter
 *   penulisan variabel pemisah = _
 *   penulisan variabel untuk dipakai disemua halaman menggunakan huruf besar semua contoh $USER;
 *   penulisan method huruf pertama kecil selanjutnya besar
 **/


include_once($SYSTEM['DIR_MODUL_CLASS']."/class.master_db.php");
class Group extends  masterDB
{
     var  $debug = 0; //array
     /**
     * Constructor
     */
     function Group()
     {
          parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
     }

     /**
     * periksa list group yang ada didatabase
     *
     * @return array list group
     */
     function getList($_condition,$_order,$_limit)
     {
          $sql =    "    SELECT *
                         FROM `group` 

                         {$_condition}  {$_order} {$_limit} 

                          " ;
          return $this->getResult($sql);
     }
     /**
     * periksa jumlah baris pada group
     *
     * @return integer total
     */
     function getCount($_condition)
     {
          $sql =    "    SELECT count(*) as total 
                         FROM `group` 
                         {$_condition} ";
          $aHasil =  $this->getResult($sql);
          return $aHasil[0]['total'];
     }
     /**
     * periksa list user group yang ada didatabase
     *
     * @return array list user  group
     */
     function getListUserGroup($_condition,$_order,$_limit)
     {
          $sql =    "    SELECT * 
                         FROM  `userGroup` A
                         LEFT JOIN  `group` B ON A.`groupID` = B.`groupID` 
                         LEFT JOIN `user` C ON A.`userID` = C.`userID`

                         {$_condition}  {$_order} {$_limit} 
                          " ;
          return $this->getResult($sql);
     }


     /**
     * update user group
     *
     * @return bool true/false
     */
     function update($_user_id,$_a_group)
     {
          $a_query[] = " DELETE FROM userGroup WHERE  `userID` =  '$_user_id';";
          foreach ($_a_group as $value) {
               $a_query[] = " INSERT INTO  `userGroup` (
                                        `userGroupID` ,`userID` ,`groupID` ,`status`
                                   )
                                   VALUES (
                                        NULL ,  '$_user_id',  '$value',  '1'
                                   );";
          }
          return $this->queryTransaction($a_query);
     }


     /**
     * mereturn  boolean dari parameter user dan group
     *
     * @return bool true/false
     */
     function isGroup($_user_id,$_group_id)
     {
          $a_user_group = $this->getListUserGroup(" WHERE A.userID = '$_user_id' AND A.groupID = '{$_group_id}' ", "",""); 
          if(isset($a_user_group))
          {
               if(count($a_user_group) > 0)
               {
                    return true;
               }
               else
               {
                    return false;
               }
          }
          else
          {
               return false;
          }
               

     }
}
?>