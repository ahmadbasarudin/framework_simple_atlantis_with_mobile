<?php

	//parameter
	//array $SYSTEM
	//string s_group from string database
	//string s_message
	function create_notification_to_group ($SYSTEM,$_s_group,$s_message)
	{

	    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.notification.php");
	    $oNotification = new Notification();

	    require_once($SYSTEM['DIR_MODUL_CLASS']."/class.user.php");
	    $oUser = new UserInfo();

	    /*notifikasi ke semua user admin*/
        $a_user = $oUser->getUserByGroup($_s_group);
        $result = true;
        if(isset($a_user))
        {
            if(count($a_user) > 0)
            {

                foreach ($a_user as $value) 
                {
                    $notif_user_to =  $value['userID']; 
                    $notif_title  = "";
                    $notif_message = $s_message;
                    $notif_link  = "";

                    
                    if(!$oNotification->create(
                        $notif_title ,
                        $notif_message ,
                        $notif_link ,
                        $notif_user_to ,
                        "" ,
                        "" ,
                        "" 
                    ))
                    {
                    	$result = false;
                    }
                }
            }
        } 
	    $oNotification->closeDB();
	    $oUser->closeDB();
	    return $result;
	}
?>