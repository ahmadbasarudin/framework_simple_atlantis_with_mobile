<?php

/*
|--------------------------------------------------------------------------
| Client
|--------------------------------------------------------------------------
|
|mendapatkan client addres berupa ip untuk pc dan imei untuk android
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
|    
 **/

/**
 *  class log
 *
 * @version         1.0
 * @author          basarudin
 * @created         21 april 2010
 * history
 *   - 24 april 2010 membuat log baru
 *
 *
 * digunakan untuk mendapatkan id unix   uuid dari client
 *
 *    _  - parameter
 **/


class Client
{

	/**
	* mendapatkan ip dari pengunjung
	*
	* @return array list group
	*/
	function getClientIP()
	{
		$ip;
		if (getenv("HTTP_CLIENT_IP")) $ip = getenv("HTTP_CLIENT_IP");
		else if(getenv("HTTP_X_FORWARDED_FOR")) $ip = getenv("HTTP_X_FORWARDED_FOR");
		else if(getenv("REMOTE_ADDR")) $ip = getenv("REMOTE_ADDR");
		else $ip = "unknown";
		return $ip;
	}


	// ini utk melihat browser agent
	function getUserAgent()
	{
		return $_SERVER['HTTP_USER_AGENT'];
	}

	//mendapatkan url cara pengunjung mengakses site
	function getClientUrl()
	{
		$url;
		$url = $_SERVER['REQUEST_URI'];
		if(!isset($url))
		{
			$url = "unknown";
		}
		return $url;
	}
}
?>