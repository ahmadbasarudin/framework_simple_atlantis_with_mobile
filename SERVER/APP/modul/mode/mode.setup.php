<?php
    /*
    |--------------------------------------------------------------------------
    | mode Create 
    |--------------------------------------------------------------------------
    |Form untuk entry mode
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    //require_once($SYSTEM['DIR_PATH']."/class/class.mode.php");
    //$omode = new mode();

    $JS_EXTENDED .= "
                    <script src='modul/{$MODUL}/{$MODUL}.js'></script>
                    ";
    $CSS_EXTENDED .= "
                    ";


    $s_form_input = "
                    <form id='form-create' action='' method='post'><div >
                            <!-- /.box-header -->
                            <div class='box-body'>
                                <div class='row'>
                                    <div class='col-md-6'>
                                        <button type='button' class='button-theme btn btn-lg btn-primary btn-block' record-id='light'>TERANG</button>
                                           
                                    </div>
                                    <div class='col-md-6'>
                                        <button type='button' class='button-theme btn btn-lg btn-dark btn-block' record-id='dark'>GELAP</button>
                                           
                                    </div>
                                    
                                    
                                    
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </form>";
                        
    $TITLE_MAIN = "Mode Tema";
    $TITLE_SUB = "";

    $BUTTON_ACTION = "
    ";

            
    $CONTENT_MAIN = "

    <!-- BEGIN PAGE CONTENT -->


        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>

                        <div class='col-md-12'>
                            <div class='card'>
                                <div class='card-header'>
                                    <div class='card-title'>Pilih Salah Satu Tema</div>
                                </div>
                                <div class='card-body'>
                                    <div >
                                        {$s_form_input}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <!-- END PAGE CONTENT -->
              ";
    //$omode->closeDB();

?>