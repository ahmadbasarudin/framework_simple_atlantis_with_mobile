$(document).ready(function() {

    $('.button-theme').click(function() 
    {
        theme_id = $(this).attr('record-id');

        $.ajax
        ({
            type: 'POST',
            url: "index.php?page=mode&type=model&action=theme&theme_id="+theme_id,
            dataType:"json",
            //data: $("#form-create").serialize(),
            cache:false,
            //jika complete maka
            complete:
                function(data,status)
                {
                 return false;
                },
            success:
                function(msg,status)
                {
                    //alert(msg);
                    if(msg.status == 'success')
                    {


                        Swal.fire
                        ({
                            type: 'success',
                            title: '',
                            text: msg.desc,
                            timer: 1200,
                            onOpen: () => 
                            {
                                var zippi = new Audio('assets/sound/success.mp3')
                                zippi.play();
                            },
                            onClose: () => 
                            {
                                document.location='index.php?page=mode';
                            }
                        });

                    }
                    else
                    {
                        
                        Swal.fire
                        ({
                            type: 'error',
                            title: '',
                            html: msg.desc,
                            onOpen: () => {
                                var zippi = new Audio('assets/sound/error.mp3')
                                zippi.play();
                            }
                        });
                    }

                },
            //untuk sementara tidak digunakan
            error:
                function(msg,textStatus, errorThrown)
                {
                    
                    Swal.fire
                    ({
                        type: 'error',
                        title: '',
                        text: 'Terjadi error saat proses kirim data',
                        onOpen: () => 
                        {
                            var zippi = new Audio('assets/sound/error.mp3')
                            zippi.play();
                        }
                    });
                }
        });//end of ajax

    });

});