
        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            errorLabelContainer : '#info',
            focusInvalid: false, // do not focus the last invalid input
            rules: 
            {
            },

            messages: 
            {
            },

            invalidHandler: function(event, validator) 
            { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).hide();
            },

            highlight: function(element) 
            { // hightlight error inputs  

                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                $('.alert-danger', $('.login-form')).hide();
            },

            success: function(label) 
            {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
                $('.alert-danger', $('.login-form')).hide();
            },

            errorPlacement: function(error, element) 
            {
                error.insertAfter(element.closest('.input-icon'));
                $('.alert-danger', $('.login-form')).hide();
            },

            submitHandler: function(form) 
            {
                $('.alert-danger').hide();
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=login&type=model",
                    dataType:"json",
                    data: $(".login-form").serialize(),
                    cache:false,
                    //jika complete maka
                    complete:
                    function(data,status)
                    {
                        return false;
                    },
                    success:
                        function(msg,status)
                        {
                            //alert(msg);
                            //jika menghasilkan result
                            if(msg.status == 'success')
                            {

                                Swal.fire({
                                    type: 'success',
                                    title: '',
                                    text: msg.desc,
                                    timer: 1200,
                                    onOpen: () => {
                                        var zippi = new Audio('assets/sound/success.mp3')
                                        zippi.play();
                                    },
                                    onClose: () => {
                                        document.location='index.php?page=welcome';
                                    }
                                })

                            }
                            else
                            {
                                Swal.fire
                                ({
                                    type: 'error',
                                    title: '',
                                    html: msg.desc,
                                    onOpen: () => {
                                        var zippi = new Audio('assets/sound/error.mp3')
                                        zippi.play();
                                    }
                                });
                            }
                        },
                   //untuk sementara tidak digunakan
                   error:
                        function(msg,textStatus, errorThrown)
                        {

                            Swal.fire
                            ({
                                type: 'error',
                                title: '',
                                text: 'Terjadi error saat proses kirim data',
                                onOpen: () => 
                                {
                                    var zippi = new Audio('assets/sound/error.mp3')
                                    zippi.play();
                                }
                            });
                        }
              });//end of ajax
              return false;
                //form.submit(); // form validation success, call ajax form submit
            }
        });


        $('.login-form input').keypress(function(e) 
        {
            if (e.which == 13)
            {
                if ($('.login-form').validate().form()) 
                {
                    $('.login-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });