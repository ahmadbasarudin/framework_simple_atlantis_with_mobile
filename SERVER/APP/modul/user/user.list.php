<?php
/**
 *
 * @version         1.0
 * @author          basarudin
 * @created     17 November 2018
 * @log
 *
 * prefix parameter:
 *    n  - node
 *    o  - object
 *    a  - array
 *    s  - string
 *    b  - boolean
 *    f  - float
 *    i  - integer
 *    fn - function
 *    _  - parameter
 *   penulisan variabel pemisah = _
 *   penulisan variabel untuk dipakai disemua halaman menggunakan huruf besar semua contoh $USER;
 *   penulisan method huruf pertama kecil selanjutnya besar
 **/

    $PAGE_ID = "USR103";
    require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    $oUser = new UserInfo();
    $oGroup = new Group();

    $BREADCRUMB['title'][] = "Pengguna";
    $BREADCRUMB['url'][] = "#";
    //---table
    $a_title  = array();
    $a_title[] = "NIP";
    $a_title[] = "NAMA";
    $a_title[] = "AVATAR";
    $a_title[] = "WILAYAH";
    $a_title[] = "STATUS";
    $a_title[] = "";
    $a_title_class[] = " style='width:100px;' ";
    $a_title_class[] = " ";
    $a_title_class[] = " style='width:10px;' ";
    $a_title_class[] = " style='width:50px;' ";
    $a_title_class[] = " style='width:50px;' ";
    $a_title_class[] = " style='width:50px;' ";

    $s_table_container = "";
    $s_condition = " WHERE true ";
    $s_limit = "  ";
    $s_order = " ";

     $a_data = $oUser->getUserList($s_condition, $s_order, $s_limit);
     if(isset($a_data))
     {
          
          $s_table_container ="";
          $s_table_container .="
                    <table  id='table-user' class='display table table-striped table-hover'   width='100%' >
                         <thead>
                              <tr >";
                                   for($i=0;$i<count($a_title);$i++)
                                   {
                                        $s_table_container .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                                   }
          $s_table_container .="</tr>
                         </thead>";
          $s_table_container .="<tbody>";



               for($i=0;$i<count($a_data);$i++)
               {
                    $s_status = "";
                    $s_button = "";
                    $s_group = "";

                    //span untuk group yang diikuti

                    $a_data_group_user =  $oGroup->getListUserGroup(" WHERE A.userID='{$a_data[$i]['userID']}' ", $s_order, $s_limit);
                    if(isset($a_data_group_user))
                    {
                         foreach ($a_data_group_user as $value) 
                         {
                            if($value['groupID'] == $GROUP_ADMIN )
                            {
                                $s_group .= "  <span class='label label-danger'>{$value['name']}</span> ";
                            }
                            elseif ($value['groupID'] ==$COMMON_GROUP) {
                                $s_group .= "";
                            }
                            else
                            {
                                $s_group .= "  <span class='label label-primary'>{$value['name']}</span> ";
                            }
                         }
                    }
                    else
                    {
                        $s_group = "belum mempunyai hak akses.";
                    }
                    if($s_group == "")
                    {
                        $s_group = "belum mempunyai hak akses.";
                    }


                    if($a_data[$i]['active'] == 0)
                    {
                         $s_status  = "<span class='badge badge-default'>belum daftar</span>";

                         $s_button = "<a class='button-aktifasi dropdown-item' href='#' record-id='{$a_data[$i]['userID']}' >Aktifkan</a>
                                                ";
                    }
                    elseif($a_data[$i]['active'] == 2)
                    {
                         $s_status  = "<span class='badge badge-danger'>non aktif</span>";
                         $s_button = "<a class='button-aktifasi dropdown-item' href='#' record-id='{$a_data[$i]['userID']}' >Aktifkan</a>
                                                ";
                    }
                    elseif($a_data[$i]['active'] == 1)
                    {

                        $s_status  = "<span class='badge badge-success'>aktif</span>";
                        $s_button = "<a class='button-deaktifasi dropdown-item' href='#' record-id='{$a_data[$i]['userID']}' >Disable</a>
                                        ";
                        $s_button .= "<a class='button-hak-akses dropdown-item' href='#' record-id='{$a_data[$i]['userID']}' >Hak Akses</a>
                                                ";
                        $s_button .= "<a class='button-photo dropdown-item' href='#' record-id='{$a_data[$i]['userID']}' >Ganti Photo Profile</a>
                                                ";
                    }

                    $BUTTON_RECORD = "
                                        <div class='btn-group'>
                                            <button type='button' class='btn btn-info btn-flat' data-toggle='dropdown' >Aksi</button>
                                            <button type='button' class='btn btn-info btn-flat dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>
                                                <span class='sr-only'>Toggle Dropdown</span>
                                            </button>
                                            <div class='dropdown-menu' role='menu' x-placement='bottom-start' style='position: absolute; transform: translate3d(87px, 43px, 0px); top: 0px; left: 0px; will-change: transform;'>
                                                {$s_button}
                                            </div>
                                        </div>
                    ";
                    //untuk informasi jumlah soal
                    $s_table_container .="<tr >";
                    $s_table_container .= "<td  align='left' >"
                                             .$a_data[$i]['userID']."<br>"
                                             .$a_data[$i]['mail']
                                        ."</td>";
                    $s_table_container .= "<td  align='left'>"
                                             .strtoupper ($a_data[$i]['realName'])."<br>".$s_group
                                        ."</td>";
                    $s_table_container .= "<td  align='center' ><img src='{$a_data[$i]['avatar']}'  class='avatar-sm avatar-img rounded-circle'></td>";
                    $s_table_container .= "<td  align='left' >"
                                             .$a_data[$i]['wilayahPenempatan']
                                        ."</td>";
                    $s_table_container .= "<td  align='left'>"
                                             .$s_status
                                        ."</td>";
                    $s_table_container .= "<td  >{$BUTTON_RECORD}</td>";
                    $s_table_container .="</tr>";
               }    
          $s_table_container .="</tbody>";
          $s_table_container .="</table>";
     }


    $JS_EXTENDED .= "
                    <!-- Datatables -->
                    <script src='assets/js/plugin/datatables/datatables.min.js'></script>


                    <script src='modul/user/user.js'></script>
                    ";
    $CSS_EXTENDED .= "
                    ";


    $TITLE_MAIN = "Pengguna";
    $TITLE_SUB = "";

    $BUTTON_ACTION = "
                                <button class='button-create-user  btn btn-success  btn-round'>
                                    <span class='btn-label'>
                                        <i class='fa fas fa-plus'></i>
                                    </span>
                                    Tambah Pengguna
                                </button>
    ";


    $CONTENT_MAIN = "

<!-- BEGIN PAGE CONTENT -->


        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>

                        <div class='col-md-12'>
                            <div class='card'>
                                <div class='card-header'>
                                    <div class='card-title'>Pengguna</div>
                                    <div class='card-category'>tabel pengguna sistem</div>
                                </div>
                                <div class='card-body'>
                                    <div class='table-responsive'>
                                        {$s_table_container}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <!-- END PAGE CONTENT -->
              ";
     $oUser->closeDB();
     $oGroup->closeDB();
?>