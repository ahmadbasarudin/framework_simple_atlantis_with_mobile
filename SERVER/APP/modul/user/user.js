$(document).ready(function() {

    if($('.select2').length)
    {
        $('.select2').select2();
    }
    
    $('#table-user tbody').on( 'click', '.button-aktifasi', function () 
    {
        user_id = $(this).attr('record-id');
        $.ajax({
        type: 'POST',
        url: "index.php?page=user&type=model&action=aktifasi",
        dataType:"json",
        data: "user-id=" +user_id,
        cache:false,
        //jika complete maka
        complete:
            function(data,status)
            {
                return false;
            },
        success:
            function(msg,status)
            {
                //alert(msg);
                if(msg.status == 'success')
                {
                    Swal.fire
                    ({
                        type: 'success',
                        title: '',
                        text: msg.desc,
                        timer: 1200,
                        onOpen: () => {
                            var zippi = new Audio('assets/sound/success.mp3')
                            zippi.play();
                        },
                        onClose: () => {
                            document.location='index.php?page=user&action=list'
                        }
                    });

                }
                else
                {
                    Swal.fire
                    ({
                        type: 'error',
                        title: '',
                        html: msg.desc,
                        onOpen: () => {
                            var zippi = new Audio('assets/sound/error.mp3')
                            zippi.play();
                        }
                    });
                }

            },
        //untuk sementara tidak digunakan
        error:
            function(msg,textStatus, errorThrown)
            {
                Swal.fire({
                    type: 'error',
                    title: '',
                    html: msg.desc,
                    onOpen: () => {
                        var zippi = new Audio('assets/sound/error.mp3')
                        zippi.play();
                    }
                });
            }
        });//end of ajax
    });

    // Handler for .ready() called.
    $('#table-user tbody').on( 'click', '.button-deaktifasi', function () 
    {
        user_id = $(this).attr('record-id');
        $.ajax({
        type: 'POST',
        url: "index.php?page=user&type=model&action=deaktifasi",
        dataType:"json",
        data: "user-id=" +user_id,
        cache:false,
        //jika complete maka
        complete:
            function(data,status)
            {
                return false;
            },
        success:
            function(msg,status)
            {
                //alert(msg);
                if(msg.status == 'success')
                {
                    Swal.fire
                    ({
                        type: 'success',
                        title: '',
                        text: msg.desc,
                        timer: 1200,
                        onOpen: () => 
                        {
                            var zippi = new Audio('assets/sound/success.mp3')
                            zippi.play();
                        },
                        onClose: () => 
                        {
                            document.location='index.php?page=user&action=list'
                        }
                    });

                }
                else
                {
                    Swal.fire
                    ({
                        type: 'error',
                        title: '',
                        html: msg.desc,
                        onOpen: () => {
                            var zippi = new Audio('assets/sound/error.mp3')
                            zippi.play();
                        }
                    });
                }

            },
        //untuk sementara tidak digunakan
        error:
            function(msg,textStatus, errorThrown)
            {
                Swal.fire
                ({
                    type: 'error',
                    title: '',
                    html: 'Terjadi kegagalan proses transaksi',
                    onOpen: () => 
                    {
                        var zippi = new Audio('assets/sound/error.mp3')
                        zippi.play();
                    }
                });
            }
        });//end of ajax
    });


    $('#table-user tbody').on( 'click', '.button-hak-akses', function () 
    {
        user_id = $(this).attr('record-id');
        document.location='index.php?page=user&action=privileges&user-id='+user_id;
    });
    $('#table-user tbody').on( 'click', '.button-photo', function () 
    {
        user_id = $(this).attr('record-id');
        document.location='index.php?page=user&action=ganti_photo&user-id='+user_id;
    });


    //check datatable untuk user dan priviliges
    if($('#table-group').length)
    {

        var table_group = $('#table-group');
        table_group.find('.group-checkable').change(function () 
        {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () 
            {
                if (checked) 
                {
                    $(this).attr("checked", true);
                    $(this).parents('tr').addClass(" table-bg-yellow ");
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('tr').removeClass(" table-bg-yellow ");
                }
            });
            jQuery.uniform.update(set);
        });

        table_group.on('change', 'tbody tr .checkboxes', function () 
        {
            $(this).parents('tr').toggleClass(" table-bg-yellow ");
        });


        jQuery('#button-privileges-apply').click(function() 
        {
            $.ajax
            ({
                    type: 'POST',
                    url: "index.php?page=user&type=model&action=update_privileges",
                    dataType:"json",
                    data: $("#form-privileges").serialize(),
                    cache:false,
                    //jika complete maka
                    complete:
                    function(data,status)
                    {
                        return false;
                    },
                    success:
                        function(msg,status){
                            //jika menghasilkan result
                            //alert(msg);
                            if(msg.status == 'success')
                            {
                                Swal.fire({
                                    type: 'success',
                                    title: '',
                                    text: msg.desc,
                                    timer: 1200,
                                    onOpen: () => {
                                        var zippi = new Audio('assets/sound/success.mp3')
                                        zippi.play();
                                    },
                                    onClose: () => {
                                        document.location='index.php?page=user&action=list'
                                    }
                                });

                            }
                            else
                            {
                                Swal.fire
                                ({
                                    type: 'error',
                                    title: '',
                                    html: msg.desc,
                                    onOpen: () => {
                                        var zippi = new Audio('assets/sound/error.mp3')
                                        zippi.play();
                                    }
                                });
                            }
                        },
                   //untuk sementara tidak digunakan
                   error:
                        function(msg,textStatus, errorThrown)
                        {

                            Swal.fire
                            ({
                                type: 'error',
                                title: '',
                                html: 'Terjadi kegagalan proses transaksi',
                                onOpen: () => {
                                    var zippi = new Audio('assets/sound/error.mp3')
                                    zippi.play();
                                }
                            });
                        }
            });//end of ajax
        });

        jQuery('.button-privileges-back').click(function() 
        {
            document.location='index.php?page=user&action=list'
        });

    }
    $('#table-group').find('.group-checkable').change(function () 
    {
        var set = jQuery(this).attr("data-set");
        var checked = jQuery(this).is(":checked");
        jQuery(set).each(function () {
            if (checked) {
                $(this).prop("checked", true);
            } else {
                $(this).prop("checked", false);
            }
        });
        jQuery.uniform.update(set);
    });


    if($('#table-user').length)
    {
        
        jQuery('.button-create-user').click(function() 
        {
            document.location='index.php?page=user&action=create'
        });
        $('#table-user').DataTable
        ({        
            rowReorder: true,
            columnDefs: [
                { orderable: true, className: 'reorder', targets:[ 3,4 ] },
                { orderable: false, targets: '_all' }
            ]
        });
    }
    if($('#button-create-user-apply').length)
    {
        $('#form-detail-user').hide();
        $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function()
        {

            $('#form-detail-user').show();
            $('html, body').animate
            ({
                scrollTop: $('#form-detail-user').offset().top
            }, 500);

            $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
            $(this).addClass('selected');
            $(this).find('input[type="radio"]').prop("checked", true);
        });

        jQuery('#button-back').click(function() 
        {
            document.location='index.php?page=user&action=list'
        });
        $('#button-create-user-apply').click(function() 
        {
            $.ajax
            ({
                type: 'POST',
                url: "index.php?page=user&type=model&action=create",
                dataType:"json",
                data: $("#form-user-create").serialize(),
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                     return false;
                    },
                success:
                    function(msg,status)
                    {
                        //alert(msg);
                        if(msg.status == 'success')
                        {
                            Swal.fire
                            ({
                                type: 'success',
                                title: '',
                                text: msg.desc,
                                timer: 1200,
                                onOpen: () => 
                                {
                                    var zippi = new Audio('assets/sound/success.mp3')
                                    zippi.play();
                                },
                                onClose: () => 
                                {
                                    document.location='index.php?page=user&action=list'
                                }
                            });

                        }
                        else
                        {
                            Swal.fire
                            ({
                                type: 'error',
                                title: '',
                                html: msg.desc,
                                onOpen: () => 
                                {
                                    var zippi = new Audio('assets/sound/error.mp3')
                                    zippi.play();
                                }
                            });
                        }
                        

                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {
                        Swal.fire
                        ({
                            type: 'error',
                            title: '',
                            text: 'Terjadi error saat proses kirim data',
                            onOpen: () => 
                            {
                                var zippi = new Audio('assets/sound/error.mp3')
                                zippi.play();
                            }
                        });
                    }
            });//end of ajax

        });
    }
    //end create user------------------------------------
    //BEGIN  GANTI PASSWORD


    if($('#button-ganti-password-apply').length)
    {
        $('#button-ganti-password-apply').click(function() 
        {
            $.ajax
            ({
                type: 'POST',
                url: "index.php?page=user&type=model&action=ganti_password",
                dataType:"json",
                data: $("#form-ganti-password").serialize(),
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                     return false;
                    },
                success:
                    function(msg,status)
                    {
                        //alert(msg);
                        if(msg.status == 'success')
                        {
                            Swal.fire({
                                type: 'success',
                                title: '',
                                text: msg.desc,
                                timer: 1200,
                                onOpen: () => 
                                {
                                    var zippi = new Audio('assets/sound/success.mp3')
                                    zippi.play();
                                },
                                onClose: () => {
                                    document.location='index.php?page=logout'
                                }
                            });

                        }
                        else
                        {
                            Swal.fire
                            ({
                                type: 'error',
                                title: '',
                                html: msg.desc,
                                onOpen: () => {
                                    var zippi = new Audio('assets/sound/error.mp3')
                                    zippi.play();
                                }
                            });
                        }

                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {
                        Swal.fire
                        ({
                            type: 'error',
                                title: '',
                            html: 'Terjadi kegagalan proses transaksi',
                            onOpen: () => 
                            {
                                var zippi = new Audio('assets/sound/error.mp3')
                                zippi.play();
                            }
                        });
                    }
            });//end of ajax

        });   
    }
    

    //END  GANTI PASSWORD
    //-------------------------------


    //BEGIN  GANTI PHOTO
    if($('#form-upload-photo').length)
    {
        user_id = $("#user-id").val();
        $("#form-upload-photo").on('submit', function(e)
        {
            //alert('asas');
            e.preventDefault();
            $.ajax({
                xhr : function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener('progress', function(e)
                    {
                        if(e.lengthComputable)
                        {
                            console.log('Bytes Loaded : ' + e.loaded);
                            console.log('Total Size : ' + e.total);
                            console.log('Persen : ' + (e.loaded / e.total));
                            
                            var percent = Math.round((e.loaded / e.total) * 100);
                            $('#progress-bar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%');
                        }
                    });
                    return xhr;
                },
                type: 'POST',
                url: "index.php?page=user&type=model&action=profilepicture",
                data: new FormData(this),
                dataType:"json",
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function()
                {
                    $('#form-upload-photo').css("opacity",".5");
                    $("#progress-bar").width('0%');
                },
                uploadProgress: function (event, position, total, percentComplete)
                { 
                    //alert('asad');
                    $("#progress-bar").width(percentComplete + '%');
                    $("#progress-bar").html('<div id="progress-status">' + percentComplete +' %</div>')
                },
                success: function(msg)
                {
                    //alert(msg['data']['filename']);
                    //alert(msg);
                    if(msg.status == 'success')
                    {
                        $('#image_profile').attr('src', msg.db_path)
                        Swal.fire
                        ({
                            type: 'success',
                            title: '',
                            text: msg.desc,
                            timer: 1200,
                            onOpen: () => {
                                var zippi = new Audio('assets/sound/success.mp3')
                                zippi.play();
                            },
                            onClose: () => {
                                document.location='index.php?page=user&action=ganti_photo&user-id='+user_id
                            }
                        });

                    }
                    else
                    {
                        Swal.fire
                        ({
                            type: 'error',
                            title: '',
                            html: msg.desc,
                            onOpen: () => {
                                var zippi = new Audio('assets/sound/error.mp3')
                                zippi.play();
                            }
                        });
                    }
                }
            });
        });
        //file type validation
        $("#file").change(function() 
        {
            var file = this.files[0];
            var image_file = file.type;
            var match= ["image/jpeg","image/png","image/jpg","video/mp4"];
            if(!((image_file==match[0]) || (image_file==match[1]) || (image_file==match[2])|| (image_file==match[3]))){

                Swal.fire
                ({
                    type: 'error',
                    title: '',
                    html: "masukan file gambar yang valid (JPEG/JPG/PNG).",
                    onOpen: () => 
                    {
                        var zippi = new Audio('assets/sound/error.mp3')
                        zippi.play();
                    }
                });
                $("#file").val('');
                return false;
            }
            $("#form-upload-photo").submit();
        });

        jQuery('#button-back').click(function() 
        {
            document.location='index.php?page=user&action=list'
        });
    };
    
    //END  GANTI PHOTO
    //-------------------------------


});