<?php
    /*
    |--------------------------------------------------------------------------
    | User
    |--------------------------------------------------------------------------
    |view  modul user
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */


    $PAGE_ID = "USR102";
    require($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    if(isset($_REQUEST['action']))
    {
        if ($_REQUEST['action'] == "list") 
        {
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.list.php");
        } 
        elseif($_REQUEST['action'] == "privileges")
        { 
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.privileges.php");
        }
        elseif($_REQUEST['action'] == "update")
        { 
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.update.php");
        }
        elseif($_REQUEST['action'] == "delete")
        { 
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.delete.php");
        }
        elseif($_REQUEST['action'] == "create")
        { 
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.create.php");
        }
        elseif($_REQUEST['action'] == "ganti_password")
        { 
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.password.php");
        }
        elseif($_REQUEST['action'] == "ganti_photo")
        { 
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.photo.php");
        }
        elseif($_REQUEST['action'] != "")
        { 
            require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.list.php");
        }
    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/{$MODUL}/{$MODUL}.list.php");
    }

?>