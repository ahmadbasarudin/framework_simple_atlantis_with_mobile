
<?php
/**
 *
 * @version		1.0
 * @author 		basarudin
 * @created     Juli 05 ,2015
 * @log
 *
 * prefix parameter:
 *    n  - node
 *    o  - object
 *    a  - array
 *    s  - string
 *    b  - boolean
 *    f  - float
 *    i  - integer
 *    fn - function
 *    _  - parameter
 *   penulisan variabel pemisah = _
 *   penulisan variabel untuk dipakai disemua halaman menggunakan huruf besar semua contoh $USER;
 *   penulisan method huruf pertama kecil selanjutnya besar
 **/




    require_once($SYSTEM['DIR_MODUL_LAYOUT']."/meta.php");
    require_once($SYSTEM['DIR_MODUL_LAYOUT']."/css.php");
    require_once($SYSTEM['DIR_MODUL_LAYOUT']."/js.php");
    
    //require_once($SYSTEM['DIR_MODUL_LAYOUT']."/sidebar.php");
    //require_once($SYSTEM['DIR_MODUL_LAYOUT']."/footer.php");

	$LAYOUT_CSS .= "
	";
	$LAYOUT_JS .= "

	";

?>
<?php

    $error_code = "";
    if(isset($_REQUEST['error_code']))
    {
        $error_code = "Kode error : <B>".$_REQUEST['error_code']."</B>";
    }
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>AKSES DITOLAK</h4>
                                </div>
                                <div style='float:right'></div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        <!-- END PAGE HEAD-->

                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>
                          <!-- Info boxes -->
                          <div class='row'>
                            <div class='error-content'>
                                <h3><i class='fa fa-warning text-warning'></i> Akses Ditolak</h3>
                                <p>
                                    Maaf, anda tidak berhak mengakses halaman ini.
                                    <br />
                                    {$error_code}
                                </p>
                            </div>
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
?>