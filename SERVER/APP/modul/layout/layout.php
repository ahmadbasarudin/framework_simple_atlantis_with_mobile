<?php
/**
 *
 * @version		1.0
 * @author 		basarudin
 * @created     Juli 05 ,2015
 * @log
 *
 * prefix parameter:
 *    n  - node
 *    o  - object
 *    a  - array
 *    s  - string
 *    b  - boolean
 *    f  - float
 *    i  - integer
 *    fn - function
 *    _  - parameter
 *   penulisan variabel pemisah = _
 *   penulisan variabel untuk dipakai disemua halaman menggunakan huruf besar semua contoh $USER;
 *   penulisan method huruf pertama kecil selanjutnya besar
 **/


    //THEME
    
    $THEME_BACKGROUND = "bg3";
    $HEADER_BACKGROUND = "blue";
    $NAVBAR_BACKGROUND = "blue2";
    $SIDEBAR_BACKGROUND = "white";
    if(isset($USER))
    {
	    if($USER[0]['theme'] == 'light')
	    {
	        $THEME_BACKGROUND = "bg3";
	        $HEADER_BACKGROUND = "blue";
	        $NAVBAR_BACKGROUND = "blue2";
	        $SIDEBAR_BACKGROUND = "white";
	    }
	    elseif($USER[0]['theme'] == 'dark')
	    {

	        $THEME_BACKGROUND = "dark";
	        $HEADER_BACKGROUND = "dark2";
	        $NAVBAR_BACKGROUND = "dark";
	        $SIDEBAR_BACKGROUND = "dark2";
	    }
    	
    }

    require_once($SYSTEM['DIR_MODUL_LAYOUT']."/meta.php");
    require_once($SYSTEM['DIR_MODUL_LAYOUT']."/css.php");
    require_once($SYSTEM['DIR_MODUL_LAYOUT']."/js.php");
    require_once($SYSTEM['DIR_MODUL_LAYOUT']."/header.php");
    require_once($SYSTEM['DIR_MODUL_LAYOUT']."/sidebar.php");
    require_once($SYSTEM['DIR_MODUL_LAYOUT']."/footer.php");
?>
<?php

	$CONTENTS = "


<!DOCTYPE html>
<html lang='en' class='topbar_open'>
	<head>
		{$LAYOUT_META}
		{$LAYOUT_CSS}
	</head>
	<body data-background-color='{$THEME_BACKGROUND}'>
	    <div class='wrapper'>
	        <div class='main-header'>
	            <!-- Logo Header -->
	            <div class='logo-header' data-background-color='{$HEADER_BACKGROUND }'>
	                
	                <a href='index.html' class='logo'>
	                    <img src='assets/img/logo.svg' alt='navbar brand' class='navbar-brand'>
	                </a>
	                <button class='navbar-toggler sidenav-toggler ml-auto' type='button' data-toggle='collapse' data-target='collapse' aria-expanded='false' aria-label='Toggle navigation'>
	                    <span class='navbar-toggler-icon'>
	                        <i class='icon-menu'></i>
	                    </span>
	                </button>
	                <button class='topbar-toggler more'><i class='icon-options-vertical'></i></button>
	                <div class='nav-toggle'>
	                    <button class='btn btn-toggle toggle-sidebar'>
	                        <i class='icon-menu'></i>
	                    </button>
	                </div>
	            </div>
	            <!-- End Logo Header -->

	            <!-- Navbar Header -->
	            <nav class='navbar navbar-header navbar-expand-sm' data-background-color='{$NAVBAR_BACKGROUND}'>
	                
	                <div class='container-fluid'>
	                    {$LAYOUT_HEADER}
	                </div>
	            </nav>
	            <!-- End Navbar -->
	        </div>

	        <!-- Sidebar -->
	        
            {$LAYOUT_SIDEBAR}
	        <!-- End Sidebar -->
	        

	        
            {$CONTENT_MAIN}


	    </div>

	    {$LAYOUT_FOOTER}
	    {$LAYOUT_JS}
	</body>
	</html>



	";



?>