package com.basar.administrator.activity;



import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basar.administrator.R;
import com.basar.administrator.helper.Config;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import libs.basar.prettydialog.PrettyDialog;
import libs.basar.prettydialog.PrettyDialogCallback;

public class WaitingActivity extends BasarActivity
{
    private static final String TAG = "waiting_aktifitas";
    private TextView button_cek_registrasi;
    private TextView text_keterangan;
    private ImageView iv_qrcode;

    private ProgressBar loading;
    private  String str_content_qrcode = "";
    private  String m_error = "";

    private  PrettyDialog m_pretty_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting);


        initComponent();
        initVariable();
        initEvent();
        mainLogic();
    }
    public void onBackPressed()
    {
        return;
    }


    public  void cekAntrian()
    {
        loading.setVisibility(View.VISIBLE);
        //jika register status = 0 dan sudah ada token firebase maka cek lagi register apakah sudah di aktifasi helpdesk
        String alamat_url = Config.getAppProtocol()+ Config.getDomain(getBaseContext())+"/index.php";
        Log.v(TAG, "alamat" + alamat_url);
        StringRequest string_request = new StringRequest(Request.Method.POST, alamat_url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        loading.setVisibility(View.GONE);
                        Log.v(TAG, "responnya :" + response);
                        try
                        {
                            JSONObject json_object = new JSONObject(response);
                            String result = json_object.getString("status");
                            Log.v(TAG, "dan hasilnya: " + result);
                            if (result.equalsIgnoreCase("success"))
                            {
                                Config.setRegisterStatus(getBaseContext(), json_object.getString("register-status"));
                                Config.setLoginToken(getBaseContext(), json_object.getString("token-login"));
                                Config.setRealname(getBaseContext(), json_object.getString("realname"));
                                m_error = "";
                                startActivity(new Intent(getBaseContext(), MainActivity.class));
                            }
                            else
                            {
                                m_error = json_object.getString("desc").toString();

                                final PrettyDialog pretty_dialog = new PrettyDialog(WaitingActivity.this);
                                pretty_dialog.setIcon(R.drawable.pdlg_icon_info).setIconTint(R.color.button_primary)
                                        .setIconCallback(new PrettyDialogCallback()
                                        {
                                            @Override
                                            public void onClick()
                                            {
                                                pretty_dialog.dismiss();
                                            }
                                        }).setMessage(m_error).setSound(R.raw.warning)
                                        .addButton(
                                                getResources().getString(R.string.dialog_button_ok),					// button text
                                                R.color.label_text,		// button text color
                                                R.color.button_primary,		// button background color
                                                new PrettyDialogCallback()
                                                {
                                                    // button OnClick listener
                                                    @Override
                                                    public void onClick()
                                                    {
                                                        pretty_dialog.dismiss();
                                                    }
                                                }
                                        ).setAnimationEnabled(true).showDialog();
                                Log.v(TAG, "versi masih rendah perlu update");
                                loading.setVisibility(View.INVISIBLE);
                            }

                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        m_error = error.toString();
                        m_pretty_dialog.setIcon(R.drawable.pdlg_icon_close).setIconTint(R.color.button_danger)
                                .setIconCallback(new PrettyDialogCallback()
                                {
                                    @Override
                                    public void onClick()
                                    {
                                        m_pretty_dialog.dismiss();
                                    }
                                }).setMessage(m_error).setSound(R.raw.error).showDialog();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("firebase-token", Config.getFirebaseToken(getBaseContext()));
                params.put("page", "login");
                params.put("type", "model");
                params.put("action", "check-waiting");

                return params;
            }
        };

        RequestQueue request_queue = Volley.newRequestQueue(this);
        //maksimal 27 detik
        string_request.setRetryPolicy(new DefaultRetryPolicy(
                Config.getVolleyMaxResponse()
                ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request_queue.add(string_request);
    }
    private  void qrCodeGenerate(String _str_barcode)
    {
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(_str_barcode, BarcodeFormat.QR_CODE, 512, 512);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            ((ImageView) findViewById(R.id.img_waiting)).setImageBitmap(bmp);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected  void initComponent()
    {
        iv_qrcode = (ImageView) findViewById(R.id.img_waiting);
        button_cek_registrasi = findViewById(R.id.button_cek_registrasi);
        loading = findViewById(R.id.loading);
        text_keterangan = findViewById(R.id.text_keterangan);
    }

    @Override
    protected  void initVariable()
    {

        m_pretty_dialog = new PrettyDialog(this);
        str_content_qrcode = Config.getFirebaseToken(getBaseContext());
        if(str_content_qrcode != "")
        {
            qrCodeGenerate(str_content_qrcode);
        }
    }

    @Override
    protected  void initEvent()
    {
        button_cek_registrasi.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void  onClick(View v){
                cekAntrian();
            }
        });
    }

    @Override
    protected  void mainLogic()
    {
        loading.setVisibility(View.GONE);
        cekAntrian();
    }
}
