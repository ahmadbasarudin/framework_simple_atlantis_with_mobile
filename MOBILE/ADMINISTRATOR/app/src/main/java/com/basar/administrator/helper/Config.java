package com.basar.administrator.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Config
{

    /** Pendeklarasian key-data berupa String, untuk sebagai wadah penyimpanan data.
     * Jadi setiap data mempunyai key yang berbeda satu sama lain */
    static final String REALNAME ="REALNAME";
    static final String LOGIN_TOKEN = "LOGIN_TOKEN";
    static final String DOMAIN = "DOMAIN";
    static final String FIREBASE_TOKEN = "FIREBASE_TOKEN";
    //0 1 dan 2
    static final String REGISTER_STATUS = "REGISTER_STATUS";

    //increment untuk versi
    static final Integer APP_VERSION = 1;
    //nama untuk versi
    static final String  APP_NAME = "com.basar.administrator";
    //protocol
    static  final String APP_PROTOCOL = "http://"; //http:// atau https://


    //lama maksimal dealy 27detik
    static final Integer APP_VOLLEY_MAX_RESPONSE = 15000;


    /** Pendlakarasian Shared Preferences yang berdasarkan paramater context */
    public static SharedPreferences getSharedPreference(Context context)
    {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /** Mengembalikan nilai dari key token berupa String */
    public static String getLoginToken(Context context)
    {
        return getSharedPreference(context).getString(LOGIN_TOKEN,"");
    }

    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key token dengan parameter _token_id */
    public static void setLoginToken(Context context, String _token_id)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(LOGIN_TOKEN, _token_id);
        editor.apply();
    }

    /** Mengembalikan nilai dari key user berupa String */
    public static String getRealname(Context context){
        return getSharedPreference(context).getString(REALNAME,"");
    }

    /** Mengembalikan nilai dari key domain berupa String */
    public static String getDomain(Context context){
        return getSharedPreference(context).getString(DOMAIN,"");
    }

    /** Mengembalikan nilai dari key token berupa String */
    public static String getFirebaseToken(Context context)
    {
        return getSharedPreference(context).getString(FIREBASE_TOKEN,"");
    }

    /** Mengembalikan nilai dari key register status berupa String */
    public static String getRegisterStatus(Context context)
    {
        return getSharedPreference(context).getString(REGISTER_STATUS,"");
    }

    /** Mengembalikan nilai nama aplikasi berupa string */
    public static String  getAppName()
    {
        return APP_NAME;
    }

    /** Mengembalikan nilai version berupa string */
    public static String  getAppProtocol()
    {
        return APP_PROTOCOL;
    }


    /** Mengembalikan nilai version berupa integer */
    public static Integer  getVersion()
    {
        return APP_VERSION;
    }


    /** Mengembalikan nilai maksimal delay volley */
    public static Integer  getVolleyMaxResponse()
    {
        return APP_VOLLEY_MAX_RESPONSE;
    }



    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key realname dengan parameter _realname */
    public static void setRealname(Context context, String _realname)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(REALNAME, _realname);
        editor.apply();
    }


    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key token dengan parameter _token_id */
    public static void setFirebaseToken(Context context, String _firebase_token)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(FIREBASE_TOKEN, _firebase_token);
        editor.apply();
    }
    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key domain dengan parameter _token_id */
    public static void setDomain(Context context, String _domain)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(DOMAIN, _domain);
        editor.apply();
    }
    /** Deklarasi Edit Preferences dan mengubah data
     *  yang memiliki key register status dengan parameter _register_status */
    public static void setRegisterStatus(Context context, String _register_status)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(REGISTER_STATUS, _register_status);
        editor.apply();
    }

    /** Deklarasi Edit Preferences dan menghapus data, sehingga menjadikannya bernilai default
     *  khusus data yang memiliki key KEY_USERNAME_SEDANG_LOGIN dan KEY_STATUS_SEDANG_LOGIN */
    public static void resetConfig(Context context)
    {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.remove(LOGIN_TOKEN);
        editor.remove(REALNAME);
        editor.remove(DOMAIN);
        editor.remove(FIREBASE_TOKEN);
        editor.remove(REGISTER_STATUS);
        editor.apply();
    }
}