package com.basar.administrator.helper;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import java.util.HashMap;
import java.util.Map;

public class InputStreamVolleyRequest extends Request<byte[]>
{
    private final Response.Listener<byte[]> m_listener;
    private Map<String, String> m_params;

    //create a static map for directly accessing headers
    public Map<String, String> response_headers ;

    public InputStreamVolleyRequest(int method, String mUrl ,Response.Listener<byte[]> listener,
                                    Response.ErrorListener errorListener, HashMap<String, String> params)
    {
        // TODO Auto-generated constructor stub
        super(method, mUrl, errorListener);
        // this request would never use cache.
        setShouldCache(false);
        m_listener = listener;
        m_params=params;
    }

    @Override
    protected Map<String, String> getParams()
            throws com.android.volley.AuthFailureError
    {
        return m_params;
    };


    @Override
    protected void deliverResponse(byte[] response)
    {
        m_listener.onResponse(response);
    }

    @Override
    protected Response<byte[]> parseNetworkResponse(NetworkResponse response)
    {

        //Initialise local response_headers map with response headers received
        response_headers = response.headers;

        //Pass the response data here
        return Response.success( response.data, HttpHeaderParser.parseCacheHeaders(response));
    }
}