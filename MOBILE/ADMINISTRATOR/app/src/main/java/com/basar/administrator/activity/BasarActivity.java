package com.basar.administrator.activity;

import androidx.appcompat.app.AppCompatActivity;

abstract class BasarActivity extends AppCompatActivity
{
    abstract  void initComponent();
    abstract  void initVariable();
    abstract  void initEvent();
    abstract  void mainLogic();
}
