package com.basar.client.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.basar.client.R;

public class ErrorActivity extends BasarActivity
{


    int MAGICAL_NUMBER = 123456;
    Button button_reboot;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);

        initComponent();
        initVariable();
        initEvent();
        mainLogic();
    }

    public void onBackPressed()
    {
        return;
    }

    private void restartApp()
    {
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        int m_pending_intent_id = MAGICAL_NUMBER;
        PendingIntent m_pending_intent = PendingIntent.getActivity(getApplicationContext(), m_pending_intent_id, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, m_pending_intent);
        System.exit(0);
    }
    @Override
    protected  void initComponent()
    {
         button_reboot = (Button) findViewById(R.id.button_reboot);
    }
    @Override
    protected  void initVariable()
    {
        if(getIntent().getExtras()!=null)
        {
            /**
             * Jika Bundle ada, ambil data dari Bundle
             */
            Bundle bundle = getIntent().getExtras();
            TextView tv_keterangan = findViewById(R.id.text_keterangan);
            tv_keterangan.setText(bundle.getString("data_error"));
        }
    }
    @Override
    protected  void initEvent()
    {
        button_reboot.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                restartApp();
            }
        });
    }
    @Override
    protected  void mainLogic()
    {
    }
}
