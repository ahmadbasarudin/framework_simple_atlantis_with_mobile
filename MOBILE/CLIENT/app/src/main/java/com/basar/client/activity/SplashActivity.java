package com.basar.client.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basar.client.R;
import com.basar.client.helper.Config;
import com.fastaccess.permission.base.PermissionHelper;
import com.fastaccess.permission.base.callback.OnPermissionCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import libs.basar.prettydialog.PrettyDialog;

public class SplashActivity extends BasarActivity  implements OnPermissionCallback, View.OnClickListener
{
    private static final String TAG = "aktifitas_splash";
    private ImageView img_splash;
    private ProgressBar loading;
    private TextView text_keterangan;
    private String m_error = "";


    private PrettyDialog m_pretty_dialog;
    private PermissionHelper permission_helper;
    private boolean is_single  = false;
    private String[] needed_permission;
    private final static String[] MULTI_PERMISSIONS = new String[]
            {
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.VIBRATE,
                    Manifest.permission.INTERNET
            };
    private AlertDialog alert_dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        initComponent();
        initVariable();
        initEvent();
        mainLogic();
    }

    @Override
    protected  void initComponent()
    {
        img_splash = findViewById(R.id.img_splash);
        loading = findViewById(R.id.loading);
        text_keterangan = findViewById(R.id.text_keterangan);
    }
    @Override
    protected  void initVariable()
    {
        
        m_pretty_dialog = new PrettyDialog(this);
        m_error = "";
    }
    @Override
    protected  void initEvent()
    {
    }
    @Override
    protected  void mainLogic()
    {
        loading.setVisibility(View.VISIBLE);
        if (periksaInternet())
        {
            Log.v(TAG, "koneksi aktif");
            if(Config.getLoginToken(getBaseContext()).equalsIgnoreCase("") )
            {
                if(Config.getRegisterStatus(getBaseContext()).equalsIgnoreCase(""))
                {
                    //jika belum pernah register maka masuk ke setting
                    startActivity(new Intent(getBaseContext(),SettingActivity.class));
                }
                else
                {
                    //jika sudah pernah register maka masuk ke form tunggu
                    startActivity(new Intent(getBaseContext(),WaitingActivity.class));
                }
            }
            else
            {
                String alamat_url = Config.getAppProtocol()+Config.getDomain(getBaseContext())+"/index.php";

                StringRequest stringRequest = new StringRequest(Request.Method.POST, alamat_url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response)
                            {
                                Log.v(TAG, "responnya :" + response);
                                try
                                {
                                    JSONObject json_object = new JSONObject(response);
                                    String result = json_object.getString("status");
                                    Log.v(TAG, "dan hasilnya: " + result);
                                    if(result.equalsIgnoreCase("success"))
                                    {
                                        Integer version = json_object.getInt("version");
                                        Log.v(TAG, "versi:"+String.valueOf(version));

                                        if(Config.getVersion()>= version)
                                        {
                                            startActivity(new Intent(getBaseContext(),MainActivity.class));
                                        }
                                        else
                                        {
                                            m_error = getResources().getString(R.string.splash_app_expired);
                                            m_pretty_dialog    .setIcon(R.drawable.pdlg_icon_close)
                                                    .setIconTint(R.color.pdlg_color_red)
                                                    .setMessage(m_error)
                                                    .setSound(R.raw.error)
                                                    .showDialog();
                                            Log.v(TAG, "versi masih rendah perlu update");
                                            loading.setVisibility(View.INVISIBLE);
                                        }
                                    }
                                    else
                                    {
                                        m_error = json_object.getString("desc").toString();
                                        m_pretty_dialog.setIcon(R.drawable.pdlg_icon_close)
                                                .setIconTint(R.color.pdlg_color_red)
                                                .setMessage(m_error)
                                                .setSound(R.raw.error)
                                                .showDialog();
                                    }

                                    Log.v(TAG, "kok g ada" );

                                }catch (JSONException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {
                                Log.v(TAG,error.toString());
                                m_error = getResources().getString(R.string.splash_connection_error);
                                m_pretty_dialog.setMessage(m_error).setSound(R.raw.error).showDialog();
                                
                                Bundle bundle = new Bundle();
                                bundle.putString("data_error", m_error);
                                Intent intent = new Intent(getBaseContext(), ErrorActivity.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                                //Toast.makeText(getApplicationContext(),"error "+error.toString(),Toast.LENGTH_LONG).show();
                            }
                        }
                )
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError
                    {
                        Map<String, String> params = new HashMap<>();
                        params.put("TOKEN_KEY", Config.getLoginToken(getBaseContext()));
                        params.put("page","app");
                        params.put("appName",Config.getAppName());
                        params.put("action","version");

                        return params;
                    }
                };
                RequestQueue request_queue = Volley.newRequestQueue(this);

                //maksimal 27 detik
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        Config.getVolleyMaxResponse()
                        , DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                        ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                request_queue.add(stringRequest);
            }
        }
        else
        {
            Log.v(TAG, "koneksi disable");
            m_error = getResources().getString(R.string.splash_connection_disable);
            m_pretty_dialog.setMessage(m_error).setSound(R.raw.error).showDialog();
        }
    }



    private boolean periksaInternet()
    {
        ConnectivityManager koneksi = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return koneksi.getActiveNetworkInfo() != null;
    }


    /*---------------begin permission helper--------------------*/
    /**
     * Used to determine if the user accepted {@link android.Manifest.permission#SYSTEM_ALERT_WINDOW} or no.
     * <p/>
     * if you never passed the permission this method won't be called.
     */
    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        permission_helper.onActivityForResult(requestCode);
    }

    @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        permission_helper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override public void onPermissionGranted(@NonNull String[] permissionName)
    {
        //result.setText("Permission(s) " + Arrays.toString(permissionName) + " Granted");
        mainLogic();
        Log.v("PERMISSION onPermissionGranted", "Permission(s) " + Arrays.toString(permissionName) + " Granted");
    }

    @Override public void onPermissionDeclined(@NonNull String[] permissionName)
    {
        //result.setText("Permission(s) " + Arrays.toString(permissionName) + " Declined");
        //result.setText("Permission(s) " + Arrays.toString(permissionName) + " Declined");
        Bundle bundle = new Bundle();
        bundle.putString("data_error", "memerlukan perizinan");
        Intent intent = new Intent(SplashActivity.this, ErrorActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        Log.i("PERMISSION onPermissionDeclined", "Permission(s) " + Arrays.toString(permissionName) + " Declined");
    }

    @Override public void onPermissionPreGranted(@NonNull String permissionsName)
    {
        //result.setText("Permission( " + permissionsName + " ) preGranted");
        Log.i("PERMISSION onPermissionPreGranted", "Permission( " + permissionsName + " ) preGranted");
    }

    @Override public void onPermissionNeedExplanation(@NonNull String permissionName)
    {
        Log.i("PERMISSION NeedExplanation", "Permission( " + permissionName + " ) needs Explanation");
        if (!is_single)
        {
            needed_permission = PermissionHelper.declinedPermissions(this, MULTI_PERMISSIONS);
            StringBuilder alert_dialog = new StringBuilder(needed_permission.length);
            if (needed_permission.length > 0)
            {
                for (String permission : needed_permission)
                {
                    alert_dialog.append(permission).append("\n");
                }
            }
            //result.setText("Permission( " + alert_dialog.toString() + " ) needs Explanation");
            AlertDialog alert = getAlertDialog(needed_permission, alert_dialog.toString());
            if (!alert.isShowing())
            {
                alert.show();
            }
        } else {
            //result.setText("Permission( " + permissionName + " ) needs Explanation");
            getAlertDialog(permissionName).show();
        }
    }

    @Override public void onPermissionReallyDeclined(@NonNull String permissionName)
    {
        //result.setText("Permission " + permissionName + " can only be granted from SettingsScreen");
        Log.v("PERMISSION ReallyDeclined", "Permission " + permissionName + " can only be granted from settingsScreen");
        /** you can call  {@link PermissionHelper#openSettingsScreen(Context)} to open the settings screen */
    }

    @Override public void onNoPermissionNeeded()
    {
        //result.setText("Permission(s) not needed");
        Log.v("PERMISSION onNoPermissionNeeded", "Permission(s) not needed");
    }

    @Override public void onClick(View v)
    {
        permission_helper
                .request(Manifest.permission.SYSTEM_ALERT_WINDOW);/*you can pass it along other permissions,
                     just make sure you override OnActivityResult so you can get a callback.
                     ignoring that will result to not be notified if the user enable/disable the permission*/
    }

    public AlertDialog getAlertDialog(final String[] permissions, final String permissionName)
    {
        if (alert_dialog == null)
        {
            alert_dialog = new AlertDialog.Builder(this)
                    .setTitle("Permission Needs Explanation")
                    .create();
        }
        alert_dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Request", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                permission_helper.requestAfterExplanation(permissions);
            }
        });
        alert_dialog.setMessage("Permissions need explanation (" + permissionName + ")");
        return alert_dialog;
    }

    public AlertDialog getAlertDialog(final String permission)
    {
        if (alert_dialog == null)
        {
            alert_dialog = new AlertDialog.Builder(this)
                    .setTitle("Permission Needs Explanation")
                    .create();
        }
        alert_dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Request", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                permission_helper.requestAfterExplanation(permission);
            }
        });
        alert_dialog.setMessage("Permission need explanation (" + permission + ")");
        return alert_dialog;
    }
    /*----------------end permission helper---------------------*/
}
