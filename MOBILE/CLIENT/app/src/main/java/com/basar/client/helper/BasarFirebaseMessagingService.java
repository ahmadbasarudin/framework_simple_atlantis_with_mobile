package com.basar.client.helper;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.basar.client.R;
import com.basar.client.activity.SplashActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
public class BasarFirebaseMessagingService extends FirebaseMessagingService
{

    private static final String TAG = "service_firebase";
    @Override
    public void onNewToken(String s)
    {
        super.onNewToken(s);
        Log.v(TAG,"token di service"+s);
    }
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        super.onMessageReceived(remoteMessage);
        // TODO(developer): Handle FCM messages here.
        Log.v(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0)
        {
            Log.v(TAG, "Message data payload: " + remoteMessage.getData());
            sendNotification("",remoteMessage.getData().get("title"),remoteMessage.getData().get("body"));
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null)
        {
            Log.v(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            sendNotification("",remoteMessage.getData().get("title"),remoteMessage.getNotification().getBody());
        }

    }
    public int createID()
    {
        Date now = new Date();
        int id = Integer.parseInt(new SimpleDateFormat("ddHHmmss",  Locale.US).format(now));
        return id;
    }

    //chanel_id string jika versi android O
    //title string
    //body string
    private void sendNotification(String _notif_id,String _title, String _body)
    {
        Log.v(TAG, "dari: " + _body);
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //PendingIntent pending_intent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationManager notification_manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //String channel_id = getString(R.string.app_name);
        Notification.Builder builder = new Notification.Builder(getBaseContext());
        builder.setContentTitle(_title);
        builder.setContentText(_body);
        builder.setSmallIcon(R.mipmap.ic_launcher);

        builder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });
        Uri sound_uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getApplicationContext().getPackageName() + "/" + R.raw.ringtone);  //Here is FILE_NAME is the name of file that you want to play


        int notif_id = createID();
        Log.v(TAG,String.valueOf(notif_id));
        AudioAttributes audio_attributes = new AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_ALARM)
                .build();

        builder.setSound(sound_uri, audio_attributes); // This is IMPORTANT
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
        {
            Log.v(TAG,"with sound");
            PendingIntent content_intent = PendingIntent.getActivity(this, 0, new Intent(this, SplashActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(content_intent);
        }//end if version sdk
        else
        {
            Log.v(TAG,"begin");
            if(sound_uri != null)
            {
                // Creating Channel
                NotificationChannel notification_channel =new NotificationChannel("CH_ID","Testing_Audio",NotificationManager.IMPORTANCE_HIGH);
                notification_channel.setSound(sound_uri,audio_attributes);
                notification_channel.enableLights(true);
                notification_channel.enableVibration(true);
                notification_manager.createNotificationChannel(notification_channel);
                Log.v(TAG,"mid2");
            }//end if sound_uri
            Log.v(TAG,"end");
        }//end else

        if(notification_manager != null)
        {
            Log.v(TAG,"notify");
            notification_manager.notify(notif_id, builder.build());
        }//enf if null
    }
}